快速开始
注：本章节以blog-web项目下开发环境为例，blog-admin项目的操作与blog-web项目类似

1. 准备工作
安装 JDK（1.8+）
安装 Maven (3.3.0+)
安装 Redis服务 (3.0+)
安装 MySQL (5.6+)
安装 IDE (Idea、Eclipse等)
以上工具，可通过【推荐软件、开发工具】下载

2. 配置项目
下载项目并导入到IDE
git clone https://gitee.com/yadong.zhang/DBlog.git
配置数据库
CREATE DATABASE dblog;
# 导入数据库`docs/db/dblog.sql`
# 初始化数据库`docs/db/init_data.sql`
修改相关配置文件
数据库链接属性(在[blog-core]/resources/config/application-center-{env}.yml配置文件中搜索datasource或定位到L.5)

redis配置(在[blog-core]/resources/config/application-center-{env}.yml配置文件中搜索redis或定位到L.14)

以上两个必备的配置项修改完成后就能启动项目了。关于其他配置项，请参考后台“系统配置”页面

3. 运行项目
方式一

$ cd DBlog
$ ./build.sh dev
$ java -jar blog-web/target/blog-web-{version}.jar
注：dev为多环境标识，可选项：dev、test和prod

dev : 开发环境 test: 测试环境 prod: 正式环境

注：{version}为当前项目的版本（非springboot），可查看对应pom文件的version节点

方式二

项目根目录下执行mvn spring-boot:run(注，如果报依赖错误，可在相关的依赖模块先执行install操作)

方式三

直接运行启动类：

web项目：BlogWebApplication.java

4. 其他信息
（web）浏览器访问http://127.0.0.1:8443
（admin）浏览器访问http://127.0.0.1:8085

后台账户

超级管理员： 账号：root 密码：123456

普通管理员： 账号：admin 密码：123456